global.db = require('../db')

var service = {
    add: function (req, res, next) {
        if (req.body) {
            global.db.add(req.body, (err, result) => {
                if (err) {
                    console.log(err)
                    res.status(400).send()
                } else {
                    res.status(200).send()
                }
            })
        } else {
            res.status(400).send()
        }
    },

    get: function (req, res, next) {
        if (typeof req.query.name != 'undefined') {
            global.db.findByName(req.query.name, (err, result) => {
                if (e) {
                    console.log(err)
                    res.status(400).send()
                } else {
                    res.status(200).send(result)
                }
            })
        } else {
            global.db.findAll((e, result) => {
                if (e) {
                    console.log(err)
                    res.status(400).send()
                } else {
                    res.status(200).send(result)
                }
            })
        }
    },

    getById: function (req, res, next) {
        if (typeof req.parameter.id != 'undefined') {
            global.db.findByName(req.parameter.id, (err, result) => {
                if (e) {
                    console.log(err)
                    res.status(400).send()
                } else {
                    res.status(200).send(result)
                }
            })
        } else {
            res.status(400).send()            
        }
    },

    delete: function (req, res, next) {
        if (req.parameter.id) {
            global.db.remove(req.parameter.id, (e, r) => {
                if (e) {
                    console.log(err)
                    res.status(400).send()
                } else {
                    res.status(200).send()
                }
            })
        } else {
            res.status(400).send()
        }
    }
}

module.exports = service