var mongoClient = require("mongodb").MongoClient;
mongoClient.connect("mongodb://localhost/starwars")
            .then(conn => global.conn = conn.db("starwars"))
            .catch(err => console.log(err))

function findAll(callback) {
    global.conn.collection("planets").find().toArray(callback);
}

function findByName(name, callback) {  
    global.conn.collection("planets").find({name: name}).toArray(callback);
}

function findById(id, callback) {  
    global.conn.collection("planets").find({id: id}).toArray(callback);
}

function add(planet, callback) {
    global.conn.collection("planets").insert(planet, callback);
}

function remove(id, callback) {
    global.conn.collection("planets").deleteOne({id: id}, callback);
}

module.exports = { findAll, findByName, findById, add, remove }