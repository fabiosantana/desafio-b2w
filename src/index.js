var express = require('express')
var app = express();
var bodyParser = require('body-parser')
const planetService = require('./services/planetService')


//CORS
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "Content-Type")
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE")
    next()
});

//Routes
app.post('/v1/planets', bodyParser.json(), planetService.add)
app.get('/v1/planets', planetService.get)
app.get('/v1/planets/:id', planetService.getById)
app.delete('/v1/planets/:id', planetService.delete)

module.exports = app

//Starting server
app.listen(3000, function() {
    console.log('Star Wars API Server started sucesfully on port 3000.')
})
