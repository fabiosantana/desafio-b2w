
const test = require('tape')
const request = require('supertest')
const index = require('./index')
const planetToTest = {
    id: 99,
    name: "TestPlanet",
    climate: "Temperate",
    terrain: "Mountains",
    films: "1"
}

test('POST /v1/planets', (t) => {
    request(index)
    .post('/v1/planets')
    .send(planetToTest)
    .expect(201)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
        assert.error(err, 'No error')
        assert.same(res.body, planetToTest, "Create a new planet")
        assert.end()
    })
})

test('GET /v1/planets', (t) => {
    request(index)
    .get('/v1/planets')
    .expect(200)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
        assert.error(err, 'No error')
        assert.end()
    })
})

test('GET /v1/planets?name=TestPlanet', (t) => {
    request(index)
    .get('/v1/planets')
    .expect(200)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
        assert.error(err, 'No error')
        assert.same(res.body, planetToTest, 'Retrieve a planet by name')
        assert.end()
    })
})

test('GET /v1/planets/99', (t) => {
    request(index)
    .get('/v1/planets/99')
    .expect(200)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
        assert.error(err, 'No error')
        assert.same(res.body, planetToTest, 'Retrieve a planet by ID')
        assert.end()
    })
})

test('DELETE /v1/planets/99', (t) => {
    request(index)
    .delete('/v1/planets/99')
    .expect(200)
    .end(function(err, res) {
        assert.error(err, 'No error')
        assert.end()
    })
})